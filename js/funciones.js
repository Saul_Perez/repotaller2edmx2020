var productos_obtenidos;
var clientes_obtenidos;
function saludar()
{
   alert("Hola");
}
function getProductos()
{
    var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
    var request = new  XMLHttpRequest();
    request.onreadystatechange = function()
    {
        if(this.readyState == 4 && this.status == 200)
        {
            productos_obtenidos = request.responseText;
            procesarProductos();
            //console.table(JSON.parse(request.responseText).value);

        }
    }
    request.open('GET',url,true);
    request.send();
}
function getClientes()
{
  var url ="https://services.odata.org/V4/Northwind/Northwind.svc/Customers?filter='conutry eq 'Germany'";
  var request = new  XMLHttpRequest();
  request.onreadystatechange = function()
  {
      if(this.readyState == 4 && this.status == 200)
      {
          clientes_obtenidos = request.responseText;
          procesarClientes();
          //console.log("entra al return");
          //console.table(JSON.parse(request.responseText).value);
      }
  }
  request.open('GET',url,true);
  request.send();
}
function procesarProductos()
{
   var json_productos = JSON.parse(productos_obtenidos);

   var etiqueta_productos = document.getElementById("table-productos");
   var tabla = document.createElement("table");
   var tbody = document.createElement("tbody");

   tabla.classList.add("table");
   tabla.classList.add("table-striped");

   var fila_titulo = document.createElement("tr");
   var columna_titulo_nombre = document.createElement("td");
   columna_titulo_nombre.innerText = "Product Name";
   var columna_titulo_company = document.createElement("td");
   columna_titulo_nombre.innerText = "Unit Price";
   var columna_titulo_phone = document.createElement("td");
   columna_titulo_nombre.innerText = "Units in stock";

   fila_titulo.appendChild(columna_titulo_nombre);
   fila_titulo.appendChild(columna_titulo_company);
   fila_titulo.appendChild(columna_titulo_phone);
   tbody.appendChild(fila_titulo);


   for(var index = 0; index < json_productos.value.length; index ++)
   {
      var fila = document.createElement("tr");

      var comlumna_nombre = document.createElement("td");
      comlumna_nombre.innerText = json_productos.value[index].ProductName;

      var columna_precio = document.createElement("td");
      columna_precio.innerText = json_productos.value[index].UnitPrice;

      var comlumna_stock = document.createElement("td");
      comlumna_stock.innerText = json_productos.value[index].UnitsInStock;

      fila.appendChild(comlumna_nombre);
      fila.appendChild(columna_precio);
      fila.appendChild(comlumna_stock);

      tbody.appendChild(fila);

   }
   tabla.appendChild(tbody);
   etiqueta_productos.appendChild(tabla);
   //alert(json_productos.value[0].ProductName);
}

function procesarClientes()
{
   var json_clientes = JSON.parse(clientes_obtenidos);
   var url_bandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
   var etiqueta_productos = document.getElementById("table-clientes");
   var tabla = document.createElement("table");
   var tbody = document.createElement("tbody");

   tabla.classList.add("table");
   tabla.classList.add("table-striped");

   for(var index = 0; index < json_clientes.value.length; index ++)
   {
      var fila = document.createElement("tr");

      var comlumna_nombre = document.createElement("td");
      comlumna_nombre.innerText = json_clientes.value[index].ContactName;

      var columna_precio = document.createElement("td");
      columna_precio.innerText = json_clientes.value[index].CompanyName;

      var comlumna_stock = document.createElement("td");

      var img_bandera = document.createElement("img");

      var bandera = json_clientes.value[index].Country;
      if(bandera == "UK")
      {
         bandera = "United-Kingdom";
      }
      img_bandera.classList.add("flag");
      //console.log("bandera: "+url_bandera+""+bandera+".png");
      img_bandera.src = url_bandera+""+bandera+".png";

      comlumna_stock.appendChild(img_bandera);
      //https://www.countries-ofthe-world.com/flags-of-the-world.html

      fila.appendChild(comlumna_nombre);
      fila.appendChild(columna_precio);
      fila.appendChild(comlumna_stock);

      tbody.appendChild(fila);

   }
   tabla.appendChild(tbody);
   etiqueta_productos.appendChild(tabla);
   //alert(json_productos.value[0].ProductName);
}

 /*alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("divTablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i = 0; i < JSONProductos.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductName;
    var columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
    var columnaStock = document.createElement("td");
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}*/
